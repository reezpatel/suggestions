import React, { Fragment, useEffect, useState } from "react";
import { getSuggestions } from "../API/suggestion";
import debounce from "../Utils/debounce";

import "./styles.scss";

const SuggestionItem = ({ text, lastTerm }) => {
  if (lastTerm) {
    const units = text.split(lastTerm);

    return (
      <Fragment>
        {units[0]}
        <span className="highlight">{lastTerm}</span>
        {units[1]}
      </Fragment>
    );
  } else {
    return text;
  }
};

const App = () => {
  const [input, setInput] = useState("");
  const [currentInput, setCurrentInput] = useState("");
  const [suggestions, setSuggestions] = useState([]);
  const [showSuggestion, setShowSuggestion] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(-1);
  const [showError, setShowError] = useState(false);

  const loadSuggestions = async (searchTerm, lastTerm) => {
    let newSuggestions = [];
    try {
      newSuggestions = await getSuggestions(lastTerm);

      if (input === searchTerm) {
        setCurrentInput(lastTerm);
        setSuggestions(newSuggestions);
        setSelectedIndex(-1);
        setShowError(false);
      }
    } catch (e) {
      console.log(e);
      setShowError(true);
      await loadSuggestions(searchTerm, lastTerm);
    }
  };

  const handleInputChange = (e) => {
    setInput(e.target.value);
  };

  useEffect(() => {
    const terms = input.split(" ");
    const lastTerm = terms[terms.length - 1];
    if (lastTerm) {
      debounce(loadSuggestions, 200, input, lastTerm)();
    } else {
      setSuggestions([]);
    }
  }, [input]);

  const handleInputFocus = () => {
    setShowSuggestion(true);
  };

  const handleInputBlur = () => {
    setShowSuggestion(false);
  };

  const handleKeyPress = (e) => {
    setShowSuggestion(true);
    switch (e.key) {
      case "ArrowDown": {
        if (selectedIndex + 1 < suggestions.length) {
          setSelectedIndex(selectedIndex + 1);
        }
        break;
      }
      case "ArrowUp": {
        if (selectedIndex - 1 > -1) {
          setSelectedIndex(selectedIndex - 1);
        }
        break;
      }
      case "Enter": {
        const terms = input.split(" ");
        const lastTerm = terms[terms.length - 1];

        if (lastTerm && selectedIndex !== -1) {
          terms[terms.length - 1] = suggestions[selectedIndex];
          setInput(terms.join(" ") + " ");
          setShowSuggestion(false);
        }
        break;
      }
      default: {
        break;
      }
    }
  };

  return (
    <div className="container">
      <div className="input-wrapper">
        <input
          type="text"
          placeholder="Type here to search"
          value={input}
          onChange={handleInputChange}
          onFocus={handleInputFocus}
          onBlur={handleInputBlur}
          onKeyDown={handleKeyPress}
        ></input>

        {suggestions.length !== 0 && showSuggestion && (
          <ul className="suggestions">
            {suggestions.map((suggestion, i) => (
              <li
                key={suggestion}
                className={selectedIndex === i ? "selected" : ""}
              >
                <SuggestionItem text={suggestion} lastTerm={currentInput} />
              </li>
            ))}
          </ul>
        )}

        {showError && (
          <div className="error">
            Something went wrong, reloading your suggestions
          </div>
        )}
      </div>
    </div>
  );
};

export default App;
