let timer;

const debounce = (fn, time, ...args) => {
  return () => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      fn(...args);
    }, time);
  };
};

export default debounce;
